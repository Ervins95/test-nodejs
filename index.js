import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel'
import cors from 'cors'

connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', { useNewUrlParser: true, useCreateIndex: true, useFindAndModify:false }) //added use 'useCreateIndex: true' remove the deprication warning

const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Just a test')
})

app.get('/users', (req, res) => {
  UserModel.find((err, results) => {
    res.send(results)
  })
})

app.post('/users', (req, res) => {
  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

app.put('/:id', async (req, res) => {
  try {
    const user = await UserModel.findByIdAndUpdate(req.params.id, req.body) //using mongoose function to find and update user info in userModel with the id specified in params. 
      res.send("Edited")
  } catch (err) {
    res.json({message:err})
  }
})

app.delete('/:id', async (req, res)=>{ //Applying async await, because a promise is being returned.
  try{
  const deleteUser = await UserModel.deleteOne({_id: req.params.id}) // Finding user by id which matches the params within UserModel and using the deleteOne function
  res.json(deleteUser)
  }catch(err){
    res.json({message:err})
  }
  

})

app.listen(8080, () => console.log('Example app listening on port 8080!'))
